# go1.13

[gov1.13源码分析](https://github.com/golang/go/tree/release-branch.go1.13)

- [encoding](/docs/encoding.md)
  - [encoding/json](/docs/encoding/json.md)
- [reflect](/docs/reflect.md)
- regexp
  - [regexp/syntax](/docs/regexp/syntax.md)
- log
  - [log](/docs/log/log.md)
  - [log_test](/docs/log/log_test.md)
